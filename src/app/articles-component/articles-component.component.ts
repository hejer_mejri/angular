import { Component, OnInit } from '@angular/core';
import { article } from '../model/article';

@Component({
  selector: 'app-articles-component',
  templateUrl: './articles-component.component.html',
  styleUrls: ['./articles-component.component.css']
})
export class ArticlesComponentComponent implements OnInit {
  titre !: string;
  listeArticles !: article[];
  text !: string;

  constructor() { }

  ngOnInit(): void {
    this.titre = "Les articles du jour : "
    this.listeArticles = [
      {
        titre: 'le championnat du monde', contenu: 'le champion du monde de cette année est   ',
        auteur: 'Med Taher', date: '12/12/2005', categorie: 'Sport'
      }, {
        titre: 'les nouveaux parents',
        contenu: 'les nouveaux parents.     ', auteur: 'Ahmed said', date: '11/11/2018',
        categorie: 'Education'
      }, {
        titre: 'Comment écrire votre CV', contenu: 'Pour réussir à décraucher un emploi...',
        auteur: 'Marie Elsa', date: '02/04/2017', categorie: 'Travail'
      },
    ]
    this.text = " "
  }

  f() {
    let nbtotal: number = 0;
    for (let i = 0; i < this.listeArticles.length; i++) {

      nbtotal++
    }
    this.text = "le nombre est :" + nbtotal
  }

}
