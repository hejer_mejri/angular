import { Injectable } from '@angular/core';
import { todo } from '../model/todo';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoServiceService {

  constructor(private myHttp : HttpClient) { }

  url : string = "https://jsonplaceholder.typicode.com/todos";


  getTodos():Observable<todo[]>{
  return this.myHttp.get<todo[]>(this.url);
  }

  getTodosByUser(userId: number): Observable<todo[]> {
    return this.myHttp.get<todo[]>(`${this.url}/todos?userId=${userId}`);
}
}
