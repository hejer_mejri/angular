import { Injectable } from '@angular/core';
import { Utilisateur } from '../model/Utilisateur';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private myHttp : HttpClient) { }

  Apiurl : string = "https://jsonplaceholder.typicode.com/users";

  getUsers():Observable<Utilisateur[]>{
    return this.myHttp.get<Utilisateur[]>(this.Apiurl);
  }
}
