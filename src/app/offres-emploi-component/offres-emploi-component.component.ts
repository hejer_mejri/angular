import { Component, OnInit } from '@angular/core';
import { Emploi } from '../model/emploi';
@Component({
  selector: 'app-offres-emploi-component',
  templateUrl: './offres-emploi-component.component.html',
  styleUrls: ['./offres-emploi-component.component.css']
})
export class OffresEmploiComponentComponent implements OnInit {
  companyName !: string;
  listeEmploi !: Emploi[];
  constructor() { }

  ngOnInit(): void {
    this.listeEmploi = [

      { reference: "cv", titre: "software programmer", entreprise: "neoXam", etat: true },
      { reference: "cv", titre: "consultant", entreprise: "orange", etat: false },
      { reference: "cv", titre: "software programmer", entreprise: "oredoo", etat: true }
    ]

  }

  calculer() {
    let nbemploi: number = 0;
    for (let i = 0; i < this.listeEmploi.length; i++) {
      if (this.listeEmploi[i].etat == true) {
        nbemploi++;
      }
    }
    console.log('Nombre emploi non cloturées =' + nbemploi);
  }



}

