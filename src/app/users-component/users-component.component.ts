import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../model/Utilisateur';
import { CalculServiceService } from '../services/calcul-service.service';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-users-component',
  templateUrl: './users-component.component.html',
  styleUrls: ['./users-component.component.css']
})
export class UsersComponentComponent implements OnInit {

  listUsers : Utilisateur[] = [];
  usernameToCheck: string = "";
  numberOfUsersWithUsername: number = 0;

  constructor(private calculService: CalculServiceService,private us : UserServiceService) { }

  ngOnInit(): void {
   /* this.listUsers=[
      {id: 1, name: "Leanne Graham", username: "Bret", email:
      "Sincere@april.biz"},
      {id: 2, name: "Ervin Howell", username: "Bret", email:
      "Shanna@melissa.tv"},
      {id: 3, name: "Clementine Bauch", username: "Samantha", email:
      "Nathan@yesenia.net"}
      ]
      */

      this.us.getUsers().subscribe(res =>{this.listUsers = res;});
  }

  checkUsername() {
    this.numberOfUsersWithUsername = this.calculService.getNumberOf(this.listUsers, 'username', this.usernameToCheck);
    alert("Nombre d'utilisateurs avec le nom d'utilisateur " + this.usernameToCheck + ": " + this.numberOfUsersWithUsername);
  }

}
