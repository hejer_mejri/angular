import { Component, OnInit} from '@angular/core';
import  { ActivatedRoute}  from '@angular/router';
import { todo } from '../model/todo';
import { CalculServiceService } from '../services/calcul-service.service';
import { TodoServiceService } from '../services/todo-service.service';

@Component({
  selector: 'app-todo-list-component',
  templateUrl: './todo-list-component.component.html',
  styleUrls: ['./todo-list-component.component.css']
})
export class TodoListComponentComponent implements OnInit {

  listTodo : todo [] = [];
  userId!: number ;
  
  completedCount : number = 0;
  constructor(private cs : CalculServiceService, private todoservice : TodoServiceService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    /*this.listTodo = [
      {userId: 1, id: 1, title: "delectus aut autem", completed:
      false},
      {userId: 1, id: 2, title: "quis ut nam facilis et officia qui", completed: false},
      {userId: 1, id: 3, title: "fugiat veniam minus", completed:
      false},
     {userId: 1, id: 4, title: "quo adipisci enim quam ut ab",
      completed: true}];
*/
      this.completedCount = this.cs.getNumberOf(this.listTodo, 'completed', true);
      

      this.todoservice.getTodos().subscribe(res=>{
        this.listTodo = res;});

        this.route.paramMap.subscribe(params => {
          this.userId = +params.get('userId')!;
          this.todoservice.getTodosByUser(this.userId).subscribe(res => {
            this.listTodo = res;
          });
        });
      
  }

  showBilan() {
    alert("Nombre de todo complétés est :" +this.completedCount)
  }

}
